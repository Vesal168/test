import React, {Component} from 'react';
import io, { Socket } from '../socket.io';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { GiftedChat } from 'react-native-gifted-chat';
import { Alert , Text} from 'react-native';

const SESSION_ID = '@sessionId';

export default class chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      sessionId: null,
    };

    this.determineUser = this.determineUser.bind(this);
    this.onReceivedMessage = this.onReceivedMessage.bind(this);
    this.onSend = this.onSend.bind(this);
    this._storeMessages = this._storeMessages.bind(this);

    this.socket = io.connect('http://localhost:1111/chat');  
    this.socket.on('message', this.onReceivedMessage);
    // this.determineUsesr();

    console.log("ok", this.socket);
    

  }

  /**
   * When a user joins the chatroom, check if they are an existing user.
   * If they aren't, then ask the server for a userId.
   * Set the userId to the component's state.
   */


  determineUser() {

    AsyncStorage.getItem(SESSION_ID)
      .then((sessionId) => {
        // Alert("Okay");
        // If there isn't a stored userId, then fetch one from the server.
        if (!sessionId) {
          this.socket.emit('userJoined', null);
          this.socket.on('userJoined', (sessionId) => {
            AsyncStorage.setItem(SESSION_ID, sessionId);
            this.setState({ sessionId });
          });
        } else {
          this.socket.emit('userJoined', sessionId);
          this.setState({ sessionId });
        }
      })
      .catch((e) => alert(e));
  }


  // Event listeners
  /**
   * When the server sends a message to this.
   */


  onReceivedMessage(messages) {
    this._storeMessages(messages);
  }

  /**
   * When a message is sent, send the message to the server
   * and store it in this component's state.
   */

  onSend(messages=[]) {
    this.socket.emit('message', messages[0]);
    this._storeMessages(messages);
  }

  render() {
    var user = { _id: this.state.sessionId || -1 };

    return (
      // <GiftedChat
      //   messages={this.state.messages}
      //   onSend={this.onSend}
      //   user={user}
      // />
      <Text style={{marginTop: 400}}>Helsffdf</Text>
    );
  }

  // Helper functions
  _storeMessages(messages) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });
  }
}